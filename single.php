<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="single-post" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<div class="row">
	<div class="medium-3 columns show-for-medium sidebar">
		<?php dynamic_sidebar( 'news_side' ); ?>
	</div>
	<div class="medium-9 columns">
		<?php while ( have_posts() ) : the_post(); ?>
			<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
				<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
					<div class="entry-content">

					<div class="single-news-slider">
							<?php /* if ( has_post_thumbnail() ) : ?>
								<div class="single-featured">
									<?php the_post_thumbnail( '', array('class' => 'th') ); ?>
								</div>
							<?php endif; */?>
							<?php if (types_render_field('news-image', array('output'=>'true'))) { ?>
									<div>
									<?=types_render_field('news-image', array('separator'=>'</div><div>')); ?>
									</div>
								</div>
							<?php } ?>
					</div>

					<header>
						<h3><?php the_title(); ?></h3>
					</header>
					<?php the_content(); ?>

					<div class="row">
						<div class="nav-previous medium-6 columns">
							<?php previous_post_link('%link', '<< Previous Post', TRUE); ?>
						</div>
						<div class="nav-next medium-6 columns">
							<?php next_post_link('%link', 'Next Post >>', TRUE); ?>
						</div>
					</div>

					<div class="row">
				        <h4><span>Other News</span></h4>
				        <div class="similar-slider" id="slider<?= $index ?>">
					        <?php
					        $categories = get_the_category();
							foreach ($categories as $index => $category):
						        $posts = get_posts(array(
						        	'post_type' => 'post',
									'showposts' => 3,
									'orderby' => rand,
									'tax_query' => array(
					 					array(
					 						'taxonomy' => 'category',
					 						'field'    => 'slug',
					 						'terms'    => $category->name
					 					)
					 				)
						        )); ?>
							   <?php //echo esc_html( $categories[0]->name ); ?>
							   <?php foreach ($posts as $post): get_post($post); ?>
									<div>
										<a href="<?= get_the_permalink(); ?>">
											<?php
			                    // If a feature image is set, get the id, so it can be injected as a css background property
			                    if ( has_post_thumbnail( $post->ID ) ) :
			                      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			                      $image = $image[0];
			                ?>
											<div class="slider-thumb" style="background-image: url('<?php echo $image ?>')">
                      	<?php endif; ?>
			                	<i class="fa fa-search"></i>
			                </div>
											<div class="slider-caption">
												<h6><?= get_the_title(); ?></h6>
											</div>
										</a>
									</div>
								<?php endforeach; ?>
							<?php endforeach; ?>
						</div>
						<?php wp_reset_query(); ?>
			    </div>
				</div>
			</article>
		<?php endwhile;?>
	</div>
</div>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>
<?php get_footer(); ?>
