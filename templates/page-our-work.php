<?php
/*
Template Name: Our Work - Main
*/
get_header(); ?>

<div id="page-our-work" role="main">
<?php do_action( 'foundationpress_before_content' ); ?>
	<div class="row">
		<div class="medium-12 columns">
			<?php while ( have_posts() ) : the_post(); ?>
			  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
<!-- 			      	<header>
			          	<h1 class="entry-title"><?php the_title(); ?></h1>
			      	</header> -->
			      	<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			      	<div class="entry-content">
			      		<section class="our-work-content">
				          	<div class="row">
								<div class="medium-4 columns">
									<a href="/commercial/">
										<div class="work-category" id="commercial">
											<h3>Commercial</h3>
										</div>
									</a>
								</div>
								<div class="medium-4 columns">
									<a href="/education/">
										<div class="work-category" id="education">
											<h3>Education</h3>
										</div>
									</a>
								</div>
								<div class="medium-4 columns">
									<a href="/healthcare/">
										<div class="work-category" id="healthcare">
											<h3>Healthcare</h3>
										</div>
									</a>
								</div>
				          	</div>
				          	<div class="row">
								<div class="medium-4 columns">
									<a href="/hospitality/">
										<div class="work-category" id="hospitality">
											<h3>Hospitality</h3>
										</div>
									</a>
								</div>
								<div class="medium-4 columns">
									<a href="/industrial/">
										<div class="work-category" id="industrial">
											<h3>Industrial</h3>
										</div>
									</a>
								</div>
								<div class="medium-4 columns">
									<a href="/multifamily/">
										<div class="work-category" id="multifamily">
											<h3>Multifamily</h3>
										</div>
									</a>
								</div>
				          	</div>
				        </section>
			      	</div>
			  	</article>
			<?php endwhile;?>
		</div>
	</div>


<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer(); ?>