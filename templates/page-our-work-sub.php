<?php
/*
Template Name: Our Work - Sub
*/
get_header(); ?>

<div id="page-our-work-sub" role="main">
<?php do_action( 'foundationpress_before_content' ); ?>
	<div class="row">
		<div class="medium-3 columns show-for-medium">
			<?php get_sidebar(); ?>
		</div>
		<div class="small-12 medium-9 columns page-content">
			<?php while ( have_posts() ) : the_post(); ?>
			  	<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
			  	<?php
			        // If a feature image is set, get the id, so it can be injected as a css background property
			        if ( has_post_thumbnail( $post->ID ) ) :
			            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			            $image = $image[0];
			            ?>

			            <header id="industry-page-title" style="background-image: url('<?php echo $image ?>')">
			        <?php endif; ?>


			        		<div class="row collapse show-for-large">
			        			<div class="industry-title-content">
				        			<div class="medium-2 columns">
				        				<div class="industry-title-icon">
					        				<?php echo types_render_field( "page-title-icon", array( ) ) ?>
					        			</div>
					        		</div>
					        		<div class="medium-6 columns">
					        			<div class="industry-title">
					          				<h1 class="entry-title"><?php the_title(); ?></h1>
					          			</div>
					          		</div>
					          		<div class="medium-4 columns">
						          		<?php if (types_render_field('page-title-red-text', array('output'=>'true'))) { ?>
							          		<div class="industry-title-caption">
												<h6>
													<span><?php echo types_render_field( "page-title-red-text", array( ) ) ?></span><br>
													<?php echo types_render_field( "page-title-white-text", array( ) ) ?>
												</h6>
							          		</div>
							          	<?php } ?>
							        </div>
							    </div>
					        </div>

							<div class="hide-for-large">
						        <div class="row collapse">
				        			<div class="industry-title-content">
					        			<div class="small-4 columns">
					        				<div class="industry-title-icon">
						        				<?php echo types_render_field( "page-title-icon", array( ) ) ?>
						        			</div>
						        		</div>
						        		<div class="small-8 columns">
						        			<div class="industry-title">
						          				<h1 class="entry-title"><?php the_title(); ?></h1>
						          			</div>
						          		</div>
						        	</div>
						       	</div>
						    </div>

			      		</header>
			      		<div class="hide-for-large">
							<div class="row collapse">
				          		<div class="small-12 columns">
					          		<?php if (types_render_field('page-title-red-text', array('output'=>'true'))) { ?>
						          		<div class="industry-title-caption">
											<h6>
												<span><?php echo types_render_field( "page-title-red-text", array( ) ) ?></span>
												<?php echo types_render_field( "page-title-white-text", array( ) ) ?>
											</h6>
						          		</div>
						          	<?php } ?>
						        </div>
					        </div>
			 			</div>
			      	<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			      	<div class="entry-content">
			          	<?php the_content(); ?>
						
						<section class="featured-project">
							<h4><span>Featured Project</span></h4>
				          	<div class="row">
			          			<?php if ( is_page('commercial') ) {
			          				$post_id = 994;
			          			} elseif ( is_page('education') ) {
			          				$post_id = 992;	
			          			} elseif ( is_page('healthcare') ) {
			          				$post_id = 990;
			          			} elseif ( is_page('hospitality') ) {
			          				$post_id = 988;
			          			} elseif ( is_page('industrial') ) {
			          				$post_id = 986;
			          			} elseif ( is_page('multifamily') ) {
			          				$post_id = 984;
			          			} else  { ?>

			          			<?php } ?>

				          		<?php $featured = new WP_Query( array( 'p' => $post_id, 'post_type' => 'featured-project', 'posts_per_page' => 1 ) ); 
				          		while( $featured->have_posts() ) : $featured->the_post(); ?>
									<div class="medium-6 columns">
										<a href="/<?php echo types_render_field( "featured-post-url", array( ) ) ?>/">
											<?php echo types_render_field( "featured-image", array( ) ) ?>
										</a>
									</div>
									<div class="medium-6 columns">
										<a href="/<?php echo types_render_field( "featured-post-url", array( ) ) ?>/">
											<h6><?php echo types_render_field( "featured-name", array( ) ) ?></h6>
										</a>
										<span class="text-bold"><?php echo types_render_field( "featured-location", array( ) ) ?></span>
										<?php echo types_render_field( "featured-description", array( ) ) ?>
										<a href="/<?php echo types_render_field( "featured-post-url", array( ) ) ?>/">Read More</a>
									</div>
								<?php endwhile; wp_reset_query(); ?>
				          	</div>
				        </section>
			          	
						<?php get_template_part( 'parts/category-sliders' ); ?>
			      	</div>
			  	</article>
			  	<div class="row">

				</div>
			<?php endwhile;?>


		</div>
	</div>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer(); ?>