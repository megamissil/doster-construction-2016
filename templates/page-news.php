<?php
/*
Template Name: News
*/
get_header(); ?>

<div id="page-news" role="main">
	<?php do_action( 'foundationpress_before_content' ); ?>
	<div class="row">
		<div class="medium-3 columns show-for-medium sidebar">
			<?php dynamic_sidebar( 'news_side' ); ?>
		</div>
		<div class="medium-9 columns">
			<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
		  	<?php
		        // If a feature image is set, get the id, so it can be injected as a css background property
		        if ( has_post_thumbnail( $post->ID ) ) :
		            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'sub-header-bg' );
		            $image = $image[0];
		            ?>

		        <header id="sub-title" style="background-image: url('<?php echo $image ?>')">
		        <?php endif; ?>
		          	<h1 class="entry-title"><?php the_title(); ?></h1>
		      	</header>

		      	<section class="news-feed">
		      	<?php
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

					$args = array ( 'category' => 2, 'posts_per_page' => 6, 'paged' => $paged );
					query_posts($args);
					if ( have_posts() ) : while (have_posts()) : the_post(); ?>
						<div class="row news-item">
					   		<div class="medium-4 columns news-img">
					   			<?php if ( has_post_thumbnail() ) { ?>
					   				<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('news-archive-img'); ?>
									</a>
								<?php } ?>
					   		</div>

					   		<div class="medium-8 columns">
					   			<a href="<?php the_permalink(); ?>">
					   				<h6><?php the_title(); ?></h6>
					   			</a>
								<span class="news-read-more"><?php echo(get_the_excerpt()); ?> <a href="<?php the_permalink(); ?>">[Read More]</a></span>
					   		</div>
						</div>
						<hr>
					<?php endwhile; ?>

					<?php the_posts_pagination( array(
						//'mid_size'  => 2,
						'prev_text' => __( 'Previous Page', 'textdomain' ),
						'next_text' => __( 'Next Page', 'textdomain' ),
					) ); ?>
					
					<?php else : ?>
					<!-- No posts found -->
					<?php endif; ?>
                </section>


			</article>
  			<?php do_action( 'foundationpress_after_content' ); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>