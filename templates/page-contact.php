<?php
/*
Template Name: Contact Us
*/
get_header(); ?>

<div id="page-contact" role="main">
	<?php do_action( 'foundationpress_before_content' ); ?>
	<?php
  // If a feature image is set, get the id, so it can be injected as a css background property
	  if ( has_post_thumbnail( $post->ID ) ) :
	    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	    $image = $image[0];
	    ?>
  	<?php endif; ?>
	<div class="row">
		<div class="medium-12 columns" id="contact-title" style="background-image: url('<?php echo $image ?>')">
			<header>
			    <h1><?php the_title(); ?></h1>
			</header>
		</div>
	</div>
	<section class="contact-content">
		<div class="row">
			<div class="medium-6 columns">
				<h4><span>Contact Information</span></h4>
				<div class="row">
					<?php dynamic_sidebar( 'contact-birmingham' ); ?>
				</div>
				<hr>
				<div class="row">
					<?php dynamic_sidebar( 'contact-nashville' ); ?>
				</div>
				<hr>
				<div class="row contact-btns">
					<div class="large-5 columns">
						<a href="mailto:info@dosterconstruction.com" class="button">Request More Information</a>
					</div>
					<div class="large-3 columns">
						<a href="/careers/" class="button">Careers</a>
					</div>
					<div class="large-4 columns">
						<a href="/portal/" class="button">Subcontractor Portal</a>
					</div>
				</div>
<!-- 				<hr>
				<a href="/portal/" class="button">Subcontractor Portal</a> -->
			</div>
			<div class="medium-6 columns">
				<?php while ( have_posts() ) : the_post(); ?>
				  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
				      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
				      <div class="contact-form">
				          <?php the_content(); ?>
				      </div>
				  </article>
				<?php endwhile;?>
			</div>
		</div>
	</section>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer(); ?>