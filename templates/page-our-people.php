<?php
/*
Template Name: Our People
*/
 get_header(); ?>

<div id="page-who-we-are" role="main">
  <?php do_action( 'foundationpress_before_content' ); ?>
  <div class="row">
    <div class="medium-3 columns side-nav">
      <?php get_sidebar(); ?>
    </div>
    <div class="medium-9 columns">
      <?php while ( have_posts() ) : the_post(); ?>
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <header>
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </article>
      <?php endwhile;?>
    </div>
  </div>


<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer(); ?>