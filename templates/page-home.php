<?php
/*
Template Name: Home
*/

get_header(); ?>

<div id="page-home" role="main">
	<article class="home-content">
		<?php putRevSlider( 'home-slider-new' ); ?>
<!-- 			<div class="home-slider">
				<?php /*
			        $args = array(
			        	'post_type' => 'home-slider-image',
			        	'showposts' => -1
			        );
		            $slider = new WP_Query( $args );
		            if( $slider->have_posts() ) {
		               	while( $slider->have_posts() ) {
		                $slider->the_post(); ?>
							<div>
								<?php echo types_render_field( "home-slider-image", array( ) ) ?>
								<div class="home-slider-caption">
									<?php echo types_render_field( "home-slider-caption", array( ) ) ?>
								</div>
							</div>
	                 	<?php
	               		}
	                } 
	            */ ?>
			</div> -->
		<?php // } ?>
		<section class="home-callout-section">
			<div class="row">
				<div class="medium-12 columns">
					<div class="row">
						<div class="medium-4 columns callout" id="home-callout-news">
							<?php $args = array(
						      	'post_type' => 'post',
						      	'showposts' => 1,
						      	'tax_query' => array(
						        	array(
						          		'taxonomy' => 'category',
						          		'field' => 'slug',
						          		'terms' => 'latest-news'
						          		
						        	)
						      	)
							); 
							$news = new WP_Query( $args );
						    if( $news->have_posts() ) {
						      	while( $news->have_posts() ) {
						        	$news->the_post(); ?>
						        	<a href="<?php the_permalink(); ?>">
										<div class="home-callout-head">
											<h4>Latest News</h4>
										</div>
									</a>

									<div class="thick-red-line"></div>							

					        		<a href="<?php the_permalink(); ?>">
										<h6><?php the_title(); ?></h6>
									</a>
									<p><?php the_excerpt(); ?></p>
									<?php
								}
							} ?>
						</div>
						<div class="medium-4 columns callout" id="home-callout-featured">
							<?php $args = array(
						      	'post_type' => 'post',
						      	'showposts' => 1,
						      	'tax_query' => array(
						        	array(
						          		'taxonomy' => 'category',
						          		'field' => 'slug',
						          		'terms' => 'featured-projects'
						          		
						        	)
						      	)
						    ); 
						    $featured = new WP_Query( $args );
						    if( $featured->have_posts() ) {
						      	while( $featured->have_posts() ) {
						        	$featured->the_post(); ?>
							        	<a href="<?php the_permalink(); ?>">
											<div class="home-callout-head">						
												<h4>Featured Projects</h4>
											</div>
										</a>
										<div class="thick-red-line"></div>							

						        		<a href="<?php the_permalink(); ?>">
											<h6><?php the_title(); ?></h6>
										</a>
										<p><?php the_excerpt(); ?></p>
										
									<?php
								}
							} ?>
						</div>
						<div class="medium-4 columns callout" id="home-callout-people">
							<?php $args = array(
						      	'post_type' => 'post',
						      	'showposts' => 1,
						      	'tax_query' => array(
						        	array(
						          		'taxonomy' => 'category',
						          		'field' => 'slug',
						          		'terms' => 'our-people'
						        	)
						      	)
						    );
							$people = new WP_Query( $args );
						    if( $people->have_posts() ) {
						      	while( $people->have_posts() ) {
						        	$people->the_post(); ?>
						        		<a href="<?php the_permalink(); ?>">
											<div class="home-callout-head">						
												<h4>Our People</h4>
											</div>
										</a>
										<div class="thick-red-line"></div>

						        		<a href="<?php the_permalink(); ?>">
											<h6><?php the_title(); ?></h6>
										</a>
										<p><?php the_excerpt(); ?></p>
										<?php
									}
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="home-industry-section">
			<div class="row">
				<?php
	                $args = array(
	                    'post_type' => 'industry',
	                    'showposts' => 6
	                );
	                  
	                $industries = new WP_Query( $args );
	                if( $industries->have_posts() ) {
	                    $i = 0;
	                    while( $industries->have_posts() ) {
	                      	$industries->the_post(); ?>
		                        <div class="medium-4 columns home-industry" id="<?=$i; ?>">
		                          	<a href="<?php echo types_render_field( "industry-page-slug", array( ) ); ?>">
		                            	<?php echo types_render_field( "home-industry-icon", array( ) ) ?>
		                            	<h5><?php echo types_render_field( "home-industry-name", array( ) ) ?></h5>
									</a>
									<p><?php echo types_render_field( "home-industry-caption", array( ) ) ?></p>
		                        </div>

		                        <?php if (($i >= 2) && ($i % 2 == 0)): ?>
		                          </div><!-- /.row -->
		                          <div class="row">
		                        <?php endif ?>
		                        <?php if ($i == 2) {
		                            $i++;
		                            $i = 0;
		                        } else {
		                            $i++;
		                         } 
	                    } 
	                } ?>
			</div>
		</section>
	</article>
</div>

<?php get_footer(); ?>
