<?php
/*
Template Name: Splash
*/
get_header(); ?>

<div id="page-splash" role="main">
<?php do_action( 'foundationpress_before_content' ); ?>
	<div class="row">
		<div class="medium-10 medium-centered columns text-center">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Doster-logo2.png" alt="logo">
			<h3>Under Construction</h3>

			<section class="splash-content">
				<div class="row">
					<div class="medium-6 columns">
						<div class="row">
							<div class="small-6 columns">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3330.4545105956736!2d-86.7590196848017!3d33.411392880785066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88891813dfa5cc19%3A0x8846c8a67ce55a5b!2s2100+International+Park+Dr%2C+Birmingham%2C+AL+35243!5e0!3m2!1sen!2sus!4v1456424697826" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<div class="small-6 columns">
								<h6>Birmingham</h6>
								<p>Corporate Headquarters<br>
								2100 International Park Drive<br>
								Birmingham, Alabama 35243<br>
								Phone: <a class="show-for-small-only" href="tel:2054433800">205.443.3800</a>
									   <span class="show-for-medium">205.443.3800</span><br>
								Fax: 205.951.2612</p>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="small-6 columns">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3226.3928732456848!2d-86.80026808434354!3d36.03511658011323!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88647b39e834531b%3A0x7ff5fce92a67d3bd!2s6+Cadillac+Dr+%23240%2C+Brentwood%2C+TN+37027!5e0!3m2!1sen!2sus!4v1456424747693" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<div class="small-6 columns">
								<h6>Nashville</h6>
								<p>6 Cadillac Drive<br>
								Suite 240<br>
								Brentwood, Tennessee 37027<br>
								Phone: <a class="show-for-small-only" href="tel:6154680404">615.468.0404</a>
									   <span class="show-for-medium">615.468.0404</span>	
								</p>
							</div>
						</div>
					</div>
					<div class="medium-6 columns">
						<?php while ( have_posts() ) : the_post(); ?>
						  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
						      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
						      <div class="contact-form">
						          <?php the_content(); ?>
						      </div>
						  </article>
						<?php endwhile;?>
					</div>
				</div>
			</section>
		</div>
	</div>


<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer(); ?>