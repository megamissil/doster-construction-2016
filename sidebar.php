<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<aside class="sidebar">
    <?php 
        if (is_page('portal')) {
          wp_nav_menu( array(
              'menu' => 'Subcontractor Portal'
          ) ); 

        } else {
			global $post; // Setup the global variable $post
			$parent_title = get_the_title($post->post_parent);

			if ( is_page() && $post->post_parent ) {
				// Make sure we are on a page and that the page is a parent.
				$kids = wp_list_pages( 'sort_column=menu_order&sort_order=asc&title_li=&child_of=' . $post->post_parent . '&echo=0' );	
			} else {
				$kids = wp_list_pages( 'sort_column=menu_order&sort_order=asc&title_li=&child_of=' . $post->ID . '&echo=0' );
			}
			if ( $kids ) {
				echo '<h5>';
					echo '<span>';
						echo $parent_title;
					echo '</span>';
				echo '</h5>';
				echo '<ul class="secondary">';
					echo $kids;
				echo '</ul>';
			}
        } 
    ?>

	<div class="side-nav-news">
		<h5><span>Recent News</span></h5>
		<?php query_posts('cat=latest-news&showposts=3'); ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		   	<div class="row collapse news-item">
		   		<div class="large-5 columns show-for-large">
		   			<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail('side-nav-news-img');
					} ?>
		   		</div>
		   		<div class="medium-12 large-7 columns">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<div class="clearfix"></div>
		   		</div>
			</div>
		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>
	</div>
	<?php 
		if ( is_page('careers') || is_page('building-information-modeling') ) {
			dynamic_sidebar( 'careers_side' );
	 	} else if ( is_page('our-approach') || $post->post_parent == '238' ) {
	 		dynamic_sidebar( 'our_approach_side' );
		} else if ( is_single() ) {
			dynamic_sidebar( 'single_side' );
		} else {
		dynamic_sidebar( 'general_side' );
		} 
	?>
</aside>
