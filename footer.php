<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<footer id="footer">
				<div class="row">
					<div class="medium-6 columns ft-description">
						<div class="row collapse hide-for-small-only">
							<div class="medium-2 large-1 columns">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/47-years.png" alt="45">
							</div>
							<div class="medium-10 large-11 columns">
								<?php dynamic_sidebar( 'footer-description' ); ?>
							</div>
						</div>
						<div class="row hide-for-medium">
							<?php dynamic_sidebar( 'footer-description' ); ?>
						</div>
						<ul class="social-icons">
							<li><a href="https://www.facebook.com/Doster-Construction-Company-253510838033011/?fref=ts" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
							<li><a href="https://www.instagram.com/dosterconstruction/" target="_blank"><i class="fa fa-instagram"></i></a></li>
							<li><a href="https://www.linkedin.com/company/231267?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A231267%2Cidx%3A2-1-8%2CtarId%3A1457463693516%2Ctas%3Adoster" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
							<li><a href="https://twitter.com/DosterInc" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UC9o8YCRXgt6JFnHRIiwkBTA" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
						</ul>
					</div>
					<div class="medium-6 columns ft-address">
						<?php dynamic_sidebar( 'footer-address' ); ?>
						<ul class="ft-links">
							<li><a href="mailto:info@dosterconstruction.com" class="ft-button">Email Doster</a></li>
							<li><a href="/portal/" class="ft-button">Subcontractor Portal</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="medium-6 columns copy">
						<p>Copyright &copy; <?=date('Y'); ?> Doster Construction. All Rights Reserved.</p>
					</div>
					<div class="medium-6 columns">
						<a href="http://digmoxy.com" id="ft-moxy">Moxy</a>
					</div>
				</div>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<?php wp_footer(); ?>


<?php do_action( 'foundationpress_before_closing_body' ); ?>

<script>
// $(document).ready(function(){
//     $(".slick-slide").mouseenter(function(){
//         $("<i class='fa fa-arrow-circle-right'></i>").insertBefore('img');
//     });

//     $(".slick-slide").mouseleave(function(){
//         $("<i class='fa fa-arrow-circle-right'></i>").remove();
//     });
// });
</script>

</body>
</html>
