<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 


<div id="page-default" role="main">
  <?php do_action( 'foundationpress_before_content' ); ?>
  <div class="row">
    <div class="medium-3 columns side-nav show-for-medium">
        <?php get_sidebar(); ?>
    </div>
    <div class="medium-9 columns page-content">
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
          <?php
          // If a feature image is set, get the id, so it can be injected as a css background property
          if ( has_post_thumbnail( $post->ID ) ) :
            //$size = apply_filters('post_thumbnail_size', 'sub-header-bg');
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'sub-header-bg' );
            $image = $image[0];
            ?>

            <header id="sub-title" style="background-image: url('<?php echo $image ?>')">
          <?php endif; ?>
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
            <div class="entry-content">
                <?php the_content(); ?>
                
                <!-- Who We Are -->
                <?php if ( is_page('who-we-are') ) { ?>
                  <div class="show-for-medium text-center">
                    <?php get_template_part( 'parts/custom-infographic' ); ?>
                  </div>
                
                <!-- Our People -->
                <?php } elseif ( is_page('our-people') ) { ?>
                  <!-- Coporate Leadership Tabs -->
                  <h4><span>Corporate Leadership</span></h4>
                  <div class="row collapse our-people-tabs">
                    <div class="medium-3 columns">
                      <ul class="tabs vertical" id="leadership-tabs" data-tabs>
                        <?php $leader = new WP_Query( array( 'post_type' => 'corporate-leader', 'posts_per_page' => -1 ) ); ?>
                        <?php 
                        $i = 1;
                        while( $leader->have_posts() ) : $leader->the_post(); ?>
                          <li class="tabs-title <?php if ($i == 1): echo 'is-active'; endif; ?> post-<?php the_ID(); ?>">
                            <a href="#post-<?php the_ID(); ?>" aria-controls="home"><?php echo types_render_field( "people-name", array( ) ) ?></a>
                          </li>

                          <?php $i++; ?>  
                        <?php endwhile; wp_reset_query(); ?>
                      </ul>
                    </div>
                    <div class="medium-9 columns">
                      <div class="tabs-content vertical" data-tabs-content="leadership-tabs">
                        <?php 
                        $i = 1;
                        $people = new WP_Query( array( 'post_type' => 'corporate-leader', 'posts_per_page' => -1 ) );
                          while( $people->have_posts() ) : $people->the_post(); ?>
                            <div class="tabs-panel <?php if ($i == 1): echo 'is-active'; endif; ?>" id="post-<?php the_ID(); ?>">
                              <?php echo types_render_field( "people-image", array( "alt" => "image", "proportional" => "true" ) ) ?>
                              <h5><?php echo types_render_field( "people-name", array( ) ) ?></h5>
                              <p><span><?php echo types_render_field( "people-position", array( ) ) ?></span></p>
                              <?php echo types_render_field( "people-description", array( ) ) ?>
                            </div>

                            <?php $i++; ?>
                          <?php endwhile; wp_reset_query(); ?>
                      </div>
                    </div>
                  </div>
                  
                  <!-- Employee Spotlight Tabs -->
                  <h4><span>Employee Spotlight</span></h4>
                  <div class="row collapse our-people-tabs">
                    <div class="medium-3 columns">
                      <ul class="tabs vertical" id="spotlight-tabs" data-tabs>
                        <?php $people = new WP_Query( array( 'post_type' => 'employee-spotlight', 'posts_per_page' => -1 ) ); ?>
                        <?php 
                        $i = 1;
                        while( $people->have_posts() ) : $people->the_post(); ?>
                          <li class="tabs-title post-<?php the_ID(); ?> <?php if ($i == 1): echo 'is-active'; endif; ?>">
                            <a href="#post-<?php the_ID(); ?>" aria-controls="home"><?php echo types_render_field( "employee-name", array( ) ) ?></a>
                          </li>
                          <?php $i++; ?>
                        <?php endwhile; wp_reset_query(); ?>
                      </ul>
                    </div>
                    <div class="medium-9 columns">
                      <div class="tabs-content vertical" data-tabs-content="spotlight-tabs">
                        <?php 
                        $i = 1;
                        $people = new WP_Query( array( 'post_type' => 'employee-spotlight', 'posts_per_page' => -1 ) );
                          while( $people->have_posts() ) : $people->the_post(); ?>
                            <div class="tabs-panel <?=($i == 1) ? 'is-active' : ''?>" id="post-<?php the_ID(); ?>">
                              <div class="row">
                                <div class="medium-4 columns">
                                  <?php echo types_render_field( "employee-image", array( "alt" => "employee-image", "proportional" => "true" ) ) ?>
                                </div>
                                <div class="medium-8 columns">
                                  <h5><?php echo types_render_field( "employee-name", array( ) ) ?></h5>
                                  <p><span class="text-bold">Role:</span> <?php echo types_render_field( "employee-role", array( ) ) ?></p>
                                  <p><span class="text-bold">What I'm working on:</span> <?php echo types_render_field( "employee-work", array( 'output' => 'raw' ) ) ?></p>
                                  <p><span class="text-bold">Fun Fact:</span> <?php echo types_render_field( "employee-fun-fact", array( 'output' => 'raw' ) ) ?></p>
                                  <p><span class="text-bold">Years with Doster:</span> <?php echo types_render_field( "employee-years", array( 'output' => 'raw' ) ) ?></p>
                                  <blockquote><?php echo types_render_field( "employee-quote", array( ) ) ?></blockquote>
                                  <?php if (types_render_field('current-project', array('output'=>'true'))) { ?>
                                    <a href="http://www.dosterconstruction.com/portfolio/<?php echo types_render_field( "current-project", array( ) ) ?>" class="button">See What I'm Working On</a>
                                  <?php } ?>
                                </div>
                              </div>
                            </div>

                            <?php $i++; ?>
                          <?php endwhile; wp_reset_query(); ?>
                      </div>
                    </div>
                  </div>

                
                <!-- Our Approach -->
                <?php } elseif ( is_page('our-approach') ) { ?>
                  <div class="row our-approach-tabs">
                    <div class="entry-content">
                        <ul class="tabs" data-tabs id="method-tabs">
                          <?php $methods = new WP_Query( array( 'post_type' => 'delivery-method', 'posts_per_page' => -1 ) ); ?>
                          <?php 
                          $i = 1;
                          while( $methods->have_posts() ) : $methods->the_post(); ?>
                            <li class="tabs-title post-<?php the_ID(); ?> <?php if ($i == 1): echo 'is-active'; endif; ?>">
                              <a href="#post-<?php the_ID(); ?>" aria-controls="home">
                                <?php echo types_render_field( "method-icon", array( "alt" => "method icon", "width" => "100", "height" => "100", "proportional" => "true" ) ) ?>
                                <?php echo types_render_field( "method-name", array( ) ) ?>
                              </a>
                          </li>
                          <?php $i++; ?>
                          <?php endwhile; wp_reset_query(); ?>
                        </ul>
                        <div class="tabs-content" data-tabs-content="method-tabs">
                        <?php 
                          $i = 1;
                          $methods = new WP_Query( array( 'post_type' => 'delivery-method', 'posts_per_page' => -1 ) );
                          while( $methods->have_posts() ) : $methods->the_post(); ?>
                            <div class="tabs-panel <?=($i == 1) ? 'is-active' : ''?>" id="post-<?php the_ID(); ?>">
                              <?php echo types_render_field( "method-description", array( ) ) ?>
                            </div>
                          <?php $i++; ?>
                          <?php endwhile; wp_reset_query(); ?>
                        </div>
                      </div>
                  </div>
                  <p>Click <a href="http://www.dosterconstruction.com/wp-content/uploads/2014/03/Delivery-Method-Comparison-Chart.pdf" target="_blank">here</a> for a delivery method comparison and analysis.</p>
                
                <!-- Building Information Modeling -->
                <?php } elseif ( is_page('building-information-modeling') ) { ?>
                <h4><span>The Latest from our BIM Blog</span></h4>
                <div class="blog-slider">
                    <?php $args = array(
                        'post_type' => 'post',
                        'tax_query' => array(
                          array(
                            'taxonomy' => 'category',
                            'field' => 'slug',
                            'terms' => 'bim'
                          )
                        )
                      );
                      $bim = new WP_Query( $args );
                      while( $bim->have_posts() ) : $bim->the_post(); ?>
                          <div>
                            <a href="<?php the_permalink(); ?>">
                              <?php
                                // If a feature image is set, get the id, so it can be injected as a css background property
                                if ( has_post_thumbnail( $post->ID ) ) :
                                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                                  $image = $image[0];
                                  ?>

                              <div class="slider-thumb" style="background-image: url('<?php echo $image ?>')">
                                <?php endif; ?>
                                  <i class="fa fa-search"></i>
                              </div>
                              <div class="slider-caption">
                                  <h6><?php the_title(); ?></h6>
                              </div>
                            </a>
                          </div>
                        <?php endwhile; wp_reset_query(); ?>
                  </div>


                <!-- Community -->
                <?php  } elseif ( is_page('community') ) { ?>
                  <div class="community-organization-slider">
                  <?php $community = new WP_Query( array( 'post_type' => 'organization', 'posts_per_page' => -1 ) );
                    while( $community->have_posts() ) : $community->the_post(); ?>
                        <div>
                          <a href="<?php echo types_render_field( "organization-link", array( ) ) ?>">
                            <?php echo types_render_field( "organization-image", array( ) ) ?>
<!--                             <div class="slider-caption">
                                <h6><?php // echo types_render_field( "organization-name", array( ) ) ?></h6>
                            </div> -->
                          </a>
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                  </div>
                  <hr>
                  <div class="community-blog-slider">
                      <?php $args = array(
                          'post_type' => 'post',
                          'tax_query' => array(
                            array(
                              'taxonomy' => 'category',
                              'field' => 'slug',
                              'terms' => 'community'
                            )
                          )
                        );
                        $community = new WP_Query( $args );
                        if( $community->have_posts() ) {
                          while( $community->have_posts() ) {
                            $community->the_post(); ?>
                            <div>
                              <a href="<?php the_permalink(); ?>">
                                <?php
                                // If a feature image is set, get the id, so it can be injected as a css background property
                                if ( has_post_thumbnail( $post->ID ) ) :
                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                                    $image = $image[0];
                                    ?>

                                <div class="slider-thumb" style="background-image: url('<?php echo $image ?>')">
                                <?php endif; ?>
                                
                                  <?php // the_post_thumbnail(); ?>
                                  <i class="fa fa-search"></i>
                                </div>
                                <div class="slider-caption">
                                    <h6><?php the_title(); ?></h6>
                                </div>
                              </a>
                            </div>
                          <?php } ?>
                        <?php } ?>
                    </div>
      

                <!-- Careers -->
                <?php } elseif ( is_page('careers') ) {
                  $args = array(
                    'post_type' => 'career',
                    'showposts' => -1
                  );
                  $careers = new WP_Query( $args );
                  if( $careers->have_posts() ) :
                    while( $careers->have_posts() ) : $careers->the_post(); ?>
                        <ul class="accordion" data-accordion data-allow-all-closed="true">
                          <li class="accordion-item" data-accordion-item>
                            <a class="accordion-title"><?php echo types_render_field( "job-title", array( ) ) ?></a>
                            <div class="accordion-content" data-tab-content>
                              <?php echo types_render_field( "job-description", array( ) ) ?>
                              <p><span class="text-bold">Recommended Experience: </span><?php echo types_render_field( "experience", array( ) ) ?></p>
                              <p><span class="text-bold">Education/Certification(s): </span><?php echo types_render_field( "education", array( ) ) ?></p>
                              <span class="text-bold">Recommended Knowledge, Skills and Abilities: </span><?php echo types_render_field( "skills", array( ) ) ?>
                              <span class="text-bold">Working Conditions</span><?php echo types_render_field( "working-conditions", array( ) ) ?>
                            </div>
                          </li>
                        </ul>
                    <?php endwhile; ?>
                                          
                    <?php wp_reset_postdata(); ?>
                  
                  <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                  <?php endif; ?>

                
                <!-- Awards -->
                <?php } elseif ( is_page('awards') ) { 
                  $args = array(
                    'post_type' => 'award',
                    'showposts' => -1
                  );
                  $awards = new WP_Query( $args ); ?>
                  <div class="row awards">
                    <div class="medium-6 columns show-for-medium">
                        <?php echo types_render_field( "awards-page-image", array( "alt" => "awards image" ) ) ?>
                    </div>
                    <div class="small-12 medium-6 columns">
                       <?php if( $awards->have_posts() ) :
                          while( $awards->have_posts() ) : $awards->the_post(); ?>
                            <div class="row award-row" data-equalizer>
                              <div class="small-3 medium-2 columns award-amount" data-equalizer-watch>
                                  <h3><?php echo types_render_field( "award-amount", array( ) ) ?></h3>
                              </div>
                              <div class="small-9 medium-10 columns award-description" data-equalizer-watch>
                                  <h5><?php echo types_render_field( "award-description", array( ) ) ?></h5>
                              </div>
                            </div>
                          <?php endwhile; ?>
                                            
                          <?php wp_reset_postdata(); ?>
                    
                        <?php else : ?>
                          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                        <?php endif; ?>
                    </div>
                  </div>

                <?php } else { ?>

                <?php } ?>
            </div>
        </article>
    </div>
  </div>


<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer(); ?>