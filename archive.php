<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="archive-page" role="main">
	<div class="row">
		<div class="medium-3 columns show-for-medium sidebar">
			<?php dynamic_sidebar( 'news_side' ); ?>
		</div>
		<div class="medium-9 columns">
			<article class="main-content">
 			<?php if (is_month()) { ?>
				<header id="sub-title" class="archive">
		          	<h1 class="entry-title"><?php the_time('F, Y') ?></h1>
		      	</header>
		    <?php } ?>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="row news-item">
				   		<div class="medium-4 columns news-img">
				   			<?php if ( has_post_thumbnail() ) { ?>
				   				<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('news-archive-img'); ?>
								</a>
							<?php } ?>
				   		</div>

				   		<div class="medium-8 columns">
				   			<a href="<?php the_permalink(); ?>">
				   				<h6><?php the_title(); ?></h6>
				   			</a>
							<?php the_excerpt(); ?>
				   		</div>
					</div>
					<hr>
				<?php endwhile; ?>



			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			<?php endif; // End have_posts() check. ?>

			</article>
		</div>
	</div>
</div>

<?php get_footer(); ?>
