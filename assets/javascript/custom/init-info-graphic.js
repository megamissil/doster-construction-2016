// ;(function($, window, document, undefined) {
// 	var $win = $(window);
// 	var $doc = $(document);

	
// })(jQuery, window, document);

$(function(){

	$('.section-info-graphic').bind('animate', function () {
		
		$(this).find('.counter').each(function () {
			var $counter = $(this),
				start = $counter.data('start') || 0,
				integer = parseInt($counter.data('int')),
				counter_value = $counter.data('value') || '';


			jQuery({ Counter: start }).animate({ Counter: integer }, {
				duration: 8000,
				easing: 'swing',
				step: function () {
					$counter.find('span').text(Math.ceil(this.Counter) + counter_value);
				},
				complete: function () {
					$counter.find('span').text(integer + counter_value);
				}
			});
		});

	});


	if($('.section-info-graphic').size() > 0) {

        scrolledToInfoGraphic = true;

        $('.section-info-graphic > div').each(function (idx) {
              var delay = 0.4;
              var seconds = delay*idx;
         });

         var $info_graphic = $('.section-info-graphic > div');

        if ($info_graphic.is(":first-child")) {
         	$info_graphic.addClass('show');
        }

        $('.section-info-graphic').trigger('animate');

    }
});