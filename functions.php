<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */


// Hide admin bar
add_filter('show_admin_bar', '__return_false');

/** Custom Thumbnail Sizes */
add_action( 'after_setup_theme', 'custom_thumbnail_sizes' );
function custom_thumbnail_sizes(){
    add_image_size( 'side-nav-news-img', 125, 75, true );
    add_image_size( 'sub-header-bg', 800, 250, true );
    add_image_size( 'news-archive-img', 235, 150, true);
}

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/menu-walkers.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );


/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

// //Read More Button For Excerpt
// function excerpt_read_more_link( $output ) {
// 	global $post;
// 	return $output . ' <a href="' . get_permalink( $post->ID ) . '" class="more-link" title="Read More">Read More</a>';
// }
// add_filter( 'the_excerpt', 'excerpt_read_more_link' );

function remove_menus(){
	 remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'remove_menus' );

// function my_post_queries( $query ) {
//   // do not alter the query on wp-admin pages and only alter it if it's the main query
//   if (!is_admin() && $query->is_main_query()) {
//     // alter the query for the category pages 
//     if(is_category()) {
//       $query->set('posts_per_page', 6);
//     }
//   }
// }
// add_action( 'pre_get_posts', 'my_post_queries' );

function _remove_script_version( $src ){
$parts = explode( '?', $src );
return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );