<?php
/**
 * Register widget areas
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_sidebar_widgets' ) ) :
function foundationpress_sidebar_widgets() {

	register_sidebar(array(
	  'id' => 'footer-description',
	  'name' => __( 'Footer - Description', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));

    register_sidebar(array(
      'id' => 'footer-address',
      'name' => __( 'Footer - Address', 'foundationpress' ),
      'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
      'before_widget' => '<article id="%1$s" class="widget %2$s">',
      'after_widget' => '</article>',
      'before_title' => '<h6>',
      'after_title' => '</h6>',
    ));

    register_sidebar( array(
        'name'          => 'News Sidebar',
        'id'            => 'news_side',
        'before_widget' => '<div class="side-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5><span>',
        'after_title'   => '</span></h5>',
    ) );

    register_sidebar( array(
        'name'          => 'Careers Sidebar',
        'id'            => 'careers_side',
        'before_widget' => '<div class="side-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5><span>',
        'after_title'   => '</span></h5>',
    ) );

    register_sidebar( array(
    'name'          => 'Our Approach Sidebar',
    'id'            => 'our_approach_side',
    'before_widget' => '<div class="side-widget">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5><span>',
    'after_title'   => '</span></h5>',
  ));

    register_sidebar( array(
        'name'          => 'Single Post Sidebar',
        'id'            => 'single_side',
        'before_widget' => '<div class="side-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5><span>',
        'after_title'   => '</span></h5>',
    ) );

    register_sidebar( array(
        'name'          => 'General Sidebar',
        'id'            => 'general_side',
        'before_widget' => '<div class="side-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5><span>',
        'after_title'   => '</span></h5>',
    ) );

    register_sidebar(array(
      'id' => 'contact-birmingham',
      'name' => __( 'Contact - Birmingham', 'foundationpress' ),
      'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
      'before_widget' => '<article id="%1$s" class="small-6 columns widget %2$s">',
      'after_widget' => '</article>',
      'before_title' => '<h6>',
      'after_title' => '</h6>',
    ));

    register_sidebar(array(
      'id' => 'contact-nashville',
      'name' => __( 'Contact - Nashville', 'foundationpress' ),
      'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
      'before_widget' => '<article id="%1$s" class="small-6 columns widget %2$s">',
      'after_widget' => '</article>',
      'before_title' => '<h6>',
      'after_title' => '</h6>',
    ));
}

add_action( 'widgets_init', 'foundationpress_sidebar_widgets' );
endif;
?>
