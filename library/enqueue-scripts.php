<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.3.0', 'all' );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );

	// If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
	// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.3.0', true );

	// Add the comment-reply library on pages where it is necessary
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;

//hide Corporate Leadership and Employee Spotlight fields from all pages except 'Our People'
add_action('admin_enqueue_scripts', 'our_people_admin_styles');
function our_people_admin_styles() {
	$arr = array(250);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
    	echo '<style type="text/css">
            #types-child-table-page-corporate-leader {display:none;}
            #types-child-table-page-employee-spotlight {display:none;}
        </style>';
    }
}

//hide Delivery Method fields from all pages except 'Our Approach'
add_action('admin_enqueue_scripts', 'our_approach_admin_styles');
function our_approach_admin_styles() {
	$arr = array(238);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
    	echo '<style type="text/css">
          #types-child-table-page-delivery-method {display:none;}
        </style>';
    }
}


//hide Career fields from all pages except 'Careers'
add_action('admin_enqueue_scripts', 'careers_admin_styles');
function careers_admin_styles() {
	$arr = array(239);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
    	echo '<style type="text/css">
          #types-child-table-page-career {display:none;}
        </style>';
    }
}

//hide Award fields from all pages except 'Awards'
add_action('admin_enqueue_scripts', 'awards_admin_styles');
function awards_admin_styles() {
    $arr = array(241);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-award {display:none;}
        </style>';
        echo '<style type="text/css">
            #wpcf-group-awards-image {display:none;}
            </style>';
    }
}

//hide Community Organization fields from all pages except 'Community'
add_action('admin_enqueue_scripts', 'community_admin_styles');
function community_admin_styles() {
    $arr = array(243);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-organization {display:none;}
        </style>';
    }
}

//hide Featured Project fields from all pages except 'Commercial, Education, Healthcare, Hospitality, Industrial, & Multifamily '
add_action('admin_enqueue_scripts', 'featured_admin_styles');
function featured_admin_styles() {
    $arr = array(244, 255, 256, 257, 258, 259);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-featured-project {display:none;}
        </style>';
    }

    if(get_post_type() == 'page' && in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #wp-content-wrap {display:none;}
        </style>';
    }    
}

// add_action('admin_enqueue_scripts', 'revslider_meta_styles');
// function revslider_meta_styles() {
//     if(get_post_type() == 'page') {
//         echo '<style type="text/css">
//           #mymetabox_revslider_0 {display:none;}
//         </style>';
//     }
// }

add_action('admin_enqueue_scripts', 'types_admin_styles');
function types_admin_styles() {
    echo '<style type="text/css">
            #types-information-table {display:none;}
        </style>';
}


//hide Home Industry fields from all pages except 'Home'
add_action('admin_enqueue_scripts', 'home_admin_styles');
function home_admin_styles() {
    $arr = array(218);
    if(get_post_type() == 'page' && in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
            .wp-editor-wrap {display:none;}
            </style>';
    }
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-industry {display:none;}
        </style>';
    }
}

?>