<?php

// Retrieve the post's (page, technically) meta data
$post = get_post();

// Retrieve taxonomies related to this post
$categories = get_the_terms($post->ID, 'portfolio-category');

if ( is_page(257) ) { ?>
	<section>
		<h4><span>Hotel & Resort</span></h4>
		<div class="category-slider" id="hotel-resort">
			<?php $posts = get_posts(array(
				'post_type' => 'portfolio',
				'post_status' => 'publish',
				'orderby' => 'date',
				'order' => 'DESC',
				'showposts' => -1,
 				'tax_query' => array(
 					array(
 						'taxonomy' => 'portfolio-category',
 						'field'    => 'slug',
 						'terms'    => 'hotel-resort'
 					)
 				)
			));

			// Loop over posts if they exist
			if ( !empty($posts) ): ?>
				<?php foreach ($posts as $post): get_post($post); ?>
					<div>
						<a href="<?= get_the_permalink(); ?>">
							<div class="slider-thumb">
                              <?php the_post_thumbnail(); ?>
                              <i class="fa fa-search"></i>
                            </div>
							<div class="slider-caption">
								<h6><?= get_the_title(); ?></h6>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</section>

	<section>
		<h4><span>Condominium</span></h4>
		<div class="category-slider" id="condominium">
			<?php $posts = get_posts(array(
				'post_type' => 'portfolio',
				'post_status' => 'publish',
				'orderby' => 'date',
				'order' => 'DESC',
				'showposts' => -1,
 				'tax_query' => array(
 					array(
 						'taxonomy' => 'portfolio-category',
 						'field'    => 'slug',
 						'terms'    => '	condominium'
 					)
 				)
			));


			// Loop over posts if they exist
			if ( !empty($posts) ): ?>
				<?php foreach ($posts as $post): get_post($post); ?>
					<div>
						<a href="<?= get_the_permalink(); ?>">
							<div class="slider-thumb">
                              <?php the_post_thumbnail(); ?>
                              <i class="fa fa-search"></i>
                            </div>
							<div class="slider-caption">
								<h6><?= get_the_title(); ?></h6>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</section>

<?php } else {

	foreach ($categories as $index => $category):

		if ($category->parent !== 0): // This line is only necessary if you want to ignore the parent categories. Otheriwse, you can remove this statement. ?>
			<section>
				<?php echo '<h4><span>' . $category->name . '</span></h4>'; ?>
				
				<div class="category-slider" id="slider<?= $index ?>">
					<?php $posts = get_posts(array(
						'post_type' => 'portfolio',
						'post_status' => 'publish',
						'orderby' => 'date',
						'order' => 'DESC',
						'showposts' => -1,
		 				'tax_query' => array(
		 					array(
		 						'taxonomy' => 'portfolio-category',
		 						'field'    => 'slug',
		 						'terms'    => $category->slug
		 					)
		 				)
					));


					// Loop over posts if they exist
					if ( !empty($posts) ): ?>
						<?php foreach ($posts as $post): get_post($post); ?>
							<div>
								<a href="<?= get_the_permalink(); ?>">
									<div class="slider-thumb">
	                                  <?php the_post_thumbnail(); ?>
	                                  <i class="fa fa-search"></i>
	                                </div>
									<div class="slider-caption">
										<h6><?= get_the_title(); ?></h6>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				
			</section>
		<?php endif; ?>

	<?php endforeach; ?>
<?php } ?>