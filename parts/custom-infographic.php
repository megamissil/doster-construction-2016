<div class="module-wrapper">
	<div class="wipcm-text">
		<div class="section section-info-graphic">
			<div class="_head">
				<p>Headquartered in Birmingham, Alabama</p>
			</div><!-- /.head -->
				
			<div class="built">
				<div class="established counter" data-start="1900" data-int="1969">
					<p>
						Established								<span>1900</span>
					</p>
				</div><!-- /.established -->
						
				<div class="project">
					<p>4,050 Projects <span>Built</span></p>
				</div><!-- /.project -->
			</div><!-- /.built -->
				
			<div class="_state counter" data-int="30">
				<p><span>30</span> <strong>States</strong></p>
			</div><!-- /.percent -->
				
			
			<div class="_percent_counter counter" data-int="90"  data-value="%">
				<span>90</span>
						
				<p>Repeat Clients</p>
			</div><!-- /.percent -->
				
			<div class="_employees counter" data-int="200">
				<p>Over <span>200</span> Employees</p>
			</div><!-- /.percent -->
				
			<div class="_year_established">
				<p><span>2003</span>Year Established our<br />
					Total Quality Management System</p>
			</div><!-- /.year established -->
				
			<div class="_award">
				<p><strong>C</strong>onsistent <strong>R</strong>ecognition,<br />
				<strong>S.T.E.P.</strong> <strong>S</strong>afety Award</p>
			</div><!-- /.safety -->
				
			<div class="_rating_counter counter" data-int="94"  data-value="%">
				<span>94</span>
				<p>Average Client Rating<br />
				For Doster&#8217;s<br />
				Professionalism &amp; Integrity</p>
			</div><!-- /.rating -->
				
			<div class="_contractors_list">
				<h5>35</h5>
				<p>consecutive years ranked within <strong>ENR’</strong>s <strong>T</strong>op <strong>400 C</strong>ontractors <strong>L</strong>ist</p>
			</div><!-- /.contractors -->
				
			<div class="_departments">
				<p><strong>O</strong>ne of the <strong>M</strong>ost <strong>P</strong>rogressive <strong>BIM D</strong>epartments in the <strong>C</strong>ountry</p>
			</div><!-- /.head -->
		</div><!-- /.section section-info-graphic -->
	</div>
</div>