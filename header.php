<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<link href='https://fonts.googleapis.com/css?family=Rokkitt:400,700' rel='stylesheet' type='text/css'>

		<link href='<?php echo get_stylesheet_directory_uri(); ?>/style.css' rel='stylesheet' type='text/css'>

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>
	
	<!-- FB Widget -->
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
		  fjs.parentNode.insertBefore(js, fjs);
		}
		(document, 'script', 'facebook-jssdk'));
	</script>

	
	<?php if ( is_page(878) ) {
		/* Splash Page: no navigation */
	} else { ?>

		<?php do_action( 'foundationpress_after_body' ); ?>

		<?php // if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
			<div class="off-canvas-wrapper">
				<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
				<?php get_template_part( 'parts/mobile-off-canvas' ); ?>
		<?php//endif; ?>

		<?php do_action( 'foundationpress_layout_start' ); ?>

		<header id="masthead" class="site-header" role="banner">


			<div class="title-bar" data-responsive-toggle="site-navigation">
				<button type="button" data-toggle="offCanvas"><i class="fa fa-bars" aria-hidden="true"></i></button>
				<div class="title-bar-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Doster-logo2.png" alt="logo">
					</a>
				</div>

			</div>

			<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
				<div class="row show-for-medium-only">
					<div class="top-bar-center">
						<ul class="menu">
							<li class="home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Doster-logo2.png" alt="logo"></a></li>
						</ul>

						<?php foundationpress_top_bar_r(); ?>

					</div>
				</div> 
				<div class="row show-for-large">
					<div class="top-bar-left">
						<ul class="menu">
							<li class="home">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Doster-logo2.png" alt="logo">
								</a>
							</li>
						</ul>
					</div>
					<div class="top-bar-right">
						<?php foundationpress_top_bar_r(); ?>
					</div>
 				</div>
 				<div class="row hide-for-medium">
 					<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>
							<?php get_template_part( 'parts/mobile-top-bar' ); ?>
					<?php endif; ?>
				</div>
			</nav>
		</header>
	<?php } ?>

	<section class="container">
		<?php do_action( 'foundationpress_after_header' ); ?>
