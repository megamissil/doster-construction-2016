<?php
/*
Single Post Template: Single - Portfolio
Description: Single post page for Portfolio items
*/


get_header(); ?>

<div id="single-portfolio" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>

	<div class="row">
		<div class="medium-3 columns show-for-medium">
			<div class="sidebar">
				<h5><span>Our Work</span></h5>
				<ul class="secondary">
					<li><a href="/commercial/">Commercial</a></li>
					<li><a href="/education/">Education</a></li>
					<li><a href="/healthcare/">Healthcare</a></li>
					<li><a href="/hospitality/">Hospitality</a></li>
					<li><a href="/industrial/">Industrial</a></li>
					<li><a href="/multifamily/">Multifamily</a></li>
				</ul>
			</div>
			<?php get_sidebar(); ?>
		</div>
		<div class="medium-9 columns page-content">
			<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

		    	<?php if (types_render_field('portfolio-image', array('output'=>'true'))) { ?>
			    	<div class="single-portfolio-slider">
						<div>
						<?=types_render_field('portfolio-image', array('separator'=>'</div><div>')); ?>
						</div>
					</div>
				<?php } ?>
					

				<h3><?php the_title(); ?> <span>|</span> <?php echo types_render_field( "location", array( ) ) ?></h3>

			    <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			    <div class="portfolio-content">
			    	<div class="row">
			    		<div class="medium-8 columns">
			        		<?php the_content(); ?>
							
							<?php if (types_render_field('testimonial', array('output'=>'true'))) { ?>
								<blockquote>
									<div class="row collapse">
										<div class="medium-1 columns">
											<i class="fa fa-quote-left"></i>
										</div>
										<div class="medium-11 columns">
											<?php echo types_render_field( 'testimonial', array( ) ) ?>
											<p><span class="text-bold">- <?php echo types_render_field( "author", array( ) ) ?></span></p>
										</div>
									</div>
								</blockquote>
							<?php } ?>
			        	</div>
			        	<div class="medium-4 columns">
			        		<?php if (types_render_field('owner', array('output'=>'true'))) : ?>
								<h6>Owner</h6>
								<p><?php echo types_render_field( 'owner', array( ) ) ?></p>
							<?php endif; ?>
							<?php if (types_render_field('architect', array('output'=>'true'))) : ?>
								<h6>Architect</h6>
								<p><?php echo types_render_field( 'architect', array( ) ) ?></p>
							<?php endif; ?>
							<?php if (types_render_field('project-facts', array('output'=>'true'))) : ?>
								<h6>Project Facts</h6>
								<p><?php echo types_render_field( 'project-facts', array( ) ) ?></p>
							<?php endif; ?>
			        	</div>
			        </div>
			        <div class="row">
				        <h4><span>Similar Projects</span></h4>
				        <div class="similar-slider" id="slider<?= $index ?>">
					        <?php 
					        $categories = get_the_terms( $post->ID, 'portfolio-category' );
							foreach ($categories as $index => $category): 
						        $posts = get_posts(array(
						        	'post_type' => 'portfolio',
									'showposts' => 3,
									'orderby' => rand,
									'tax_query' => array(
					 					array(
					 						'taxonomy' => 'portfolio-category',
					 						'field'    => 'slug',
					 						'terms'    => $category->name
					 					)
					 				)
						        )); ?>
							   <?php //echo esc_html( $categories[0]->name ); ?>
							   <?php foreach ($posts as $post): get_post($post); ?>
									<div>
										<a href="<?= get_the_permalink(); ?>">
										<?php 
						                    if ( has_post_thumbnail( $post->ID ) ) :
						                      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
						                      $image = $image[0];
						                ?>
											<div class="slider-thumb" style="background-image: url('<?php echo $image ?>')">
										<?php endif; ?>
			                                  <?php //the_post_thumbnail('single-post-thumbnail'); ?>
			                                  <i class="fa fa-search"></i>
			                                </div>
											<div class="slider-caption">
												<h6><?= get_the_title(); ?></h6>
											</div>
										</a>
									</div>
								<?php endforeach; ?>
							<?php endforeach; ?>
						</div>
						<?php wp_reset_query(); ?>
			        </div>
			    </div>
			</article>
		</div>
	</div>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>
<?php get_footer(); ?>