<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="archive-page" role="main">
	<div class="row">
		<div class="medium-3 columns show-for-medium sidebar">
			<?php dynamic_sidebar( 'news_side' ); ?>
		</div>
		<div class="medium-9 columns">
			<article class="main-content">
				<header id="sub-title" class="archive">
		          	<h1 class="entry-title"><?php single_cat_title(); ?></h1>
		      	</header>
		      	
		      	<section class="news-feed">
			      	<?php
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

						$args = array ( 'posts_per_page' => -1, 'paged' => $paged );
						if ( have_posts() ) : while (have_posts()) : the_post(); ?>
							<div class="row news-item">
						   		<div class="medium-4 columns news-img">
						   			<?php if ( has_post_thumbnail() ) { ?>
						   				<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail('news-archive-img'); ?>
										</a>
									<?php } ?>
						   		</div>

						   		<div class="medium-8 columns">
						   			<a href="<?php the_permalink(); ?>">
						   				<h6><?php the_title(); ?></h6>
						   			</a>
									<?php the_excerpt(); ?>
						   		</div>
							</div>
							<hr>
						<?php endwhile; ?>

						<?php the_posts_pagination( array(
							//'mid_size'  => 2,
							'prev_text' => __( 'Previous Page', 'textdomain' ),
							'next_text' => __( 'Next Page', 'textdomain' ),
						) ); ?>

						<?php else : ?>
							<?php get_template_part( 'content', 'none' ); ?>
						<?php endif; // End have_posts() check. ?>

				</section>
			</article>
		</div>
	</div>
</div>

<?php get_footer(); ?>
